#! /usr/bin/env node

process.env.NODE_PATH = __dirname + "/../node_modules";
const { resolve } = require("path");
const res = command => resolve(__dirname, '../commands/', command);
const program = require("commander");
const pkg = require("../package.json");

program
    .version(pkg.version)

program
    .usage("<command>")

program
    .command("init")
    .description("Generate a new node project")
    .alias("i")
    .action(() => {
        require(res("init"))
    })

program.parse(process.argv)

if(!program.args || !program.args.length){
    program.help()
    }